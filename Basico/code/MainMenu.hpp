#pragma once

#include "SSprite.hpp"
#include "SImage.hpp"
#include <iostream>
#include "SInput.hpp"
#include <IwGxPrint.h>
#include "SButton.hpp"
#include "STexture.hpp"
#include "SAnimSprite.hpp"
#include "Shared_Ptr.hpp"
#include "SText.hpp"
#include "SAudio.hpp"
#include "STweenManager.hpp"
#include "SDisplay.hpp"

using namespace storm;
using toolkit::Shared_Ptr;

class MainMenu : public SSprite
{
public:

	MainMenu(void);
	~MainMenu(void);

	void Update		();
	void Release	();

private:

	
	Shared_Ptr< STexture >	texture;
	Shared_Ptr< STexture >	texture2;
	Shared_Ptr< STexture >	texture3;
	Shared_Ptr< SImage >	image;
	Shared_Ptr< SImage >	image2;
	Shared_Ptr< SButton >	boton;
	Shared_Ptr< STexture > 	fondo;
	Shared_Ptr< SImage >	image_fondo;
	Shared_Ptr< SText >		texto;
	Shared_Ptr< SAnimSprite> anim;
	float	rotation;
};

