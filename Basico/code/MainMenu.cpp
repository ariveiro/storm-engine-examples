#include <MainMenu.hpp>

using std::cout;
using std::endl;

MainMenu::MainMenu()
{
	fondo.reset		(new STexture("a.png"));
	texture.reset	(new STexture("test1.png"));
	texture2.reset	(new STexture("test2.png"));
	texture3.reset	(new STexture("test3.png")); //peque�a
	
	image_fondo.reset(new SImage);
	image_fondo->SetTexture(fondo);
	image.reset(new SImage);
	image->SetTexture(texture);
	image2.reset(new SImage);
	image2->SetTexture(texture2);

	boton.reset(new SButton(texture2, texture, "Boton"));

	texto.reset(new SText("Texto suelto que se adapta a un ancho y un alto", "font.gxfont", 0xff04B404, CIwFVec2(100,100)));

	// A�adimos todos los hijos a MainMenu(heredado de SSprite) 
	// para que pueda llamar a sus Update y Draw
	addChild(image);
	addChild(image2);
	addChild(boton);
	addChild(texto);

	//Posicionamos la imagen2
	image2->setX(50.0f);
	image2->setY(50.0f);
	image2->SetPivot(-image2->getWidth() / 2, -image2->getHeight() / 2);

	boton->setPosition(150.0f, 150.0f);

	rotation = 0;
	
	//Prueba colisi�n
	bool a = false;
	a = image->collides(image2->getX(),image2->getY(),image2->getWidth(),image2->getHeight());

	g_pAudio = new Audio();
}


MainMenu::~MainMenu()
{
}


void MainMenu::Update()
{
	SSprite::Update();
	
	image2->setPosition(SInput::GetInstance()->touch_x, SInput::GetInstance()->touch_y);

	++rotation;
	image2->setRotation(rotation);

	if(SInput::GetInstance()->isTouched)
		g_pAudio->PlaySound("audio/gem_destroyed.wav");
}


void MainMenu::Release()
{
	SSprite::Release();
	delete g_pAudio;
}
