#include <s3e.h>
#include <IwDebug.h>
#include <StormEngine.hpp>
#include <MainMenu.hpp>
#include "Shared_Ptr.hpp"

using storm::StormEngine;

// Main entry point for the application
int main()
{
	
	StormEngine stormEngine(1136, 640, 60.0f);

	MainMenu *mainMenu = new MainMenu();

	stormEngine.SetContainer(mainMenu);
	stormEngine.Start();

    // Return
    return 0;
}
