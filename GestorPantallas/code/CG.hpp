#ifndef _CG
#define _CG

namespace CG
{
	static enum SCREENS
	{
		MAIN_MENU_SCREEN,
		GAME_SCREEN
	};
	
	static SCREENS screens = MAIN_MENU_SCREEN;
}

#endif