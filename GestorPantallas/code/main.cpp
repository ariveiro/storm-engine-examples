#include "s3e.h"
#include "IwDebug.h"
#include "StormEngine.hpp"
#include "GameEngine.hpp"

using storm::StormEngine;

// Main entry point for the application
int main()
{
	
	StormEngine stormEngine(1024, 768, 60.0f);

	GameEngine *gamEngine = new GameEngine();

	stormEngine.SetContainer(gamEngine);
	stormEngine.Start();


    return 0;
}
