#include "GameScreen.hpp"


GameScreen::GameScreen()
{
	fondo.reset		(new STexture("a.png"));
	texture.reset	(new STexture("test1.png"));
	texture2.reset	(new STexture("test2.png"));
	texture3.reset	(new STexture("test3.png")); //peque�a

	image_fondo.reset(new SImage);
	image_fondo->SetTexture(fondo);
	image.reset(new SImage);
	image->SetTexture(texture);
	image2.reset(new SImage);
	image2->SetTexture(texture2);
	image3.reset(new SImage);
	image3->SetTexture(texture);

	addChild(image_fondo);
	addChild(image);
	addChild(image2);
	addChild(image3);
}

GameScreen::~GameScreen()
{
}


void GameScreen::Update()
{
	SSprite::Update();
}

void GameScreen::Release()
{
	SSprite::Release();
}