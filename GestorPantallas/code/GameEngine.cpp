#include "GameEngine.hpp"

bool GameEngine::needChange = false;
bool GameEngine::s_goBack   = false;
CG::SCREENS GameEngine::s_screen = CG::MAIN_MENU_SCREEN;

GameEngine::GameEngine()
{
	currentScreen.reset();
	ChangeScreen(CG::MAIN_MENU_SCREEN, false);
}

void GameEngine::EventChangeScreen(CG::SCREENS screen, bool goBack)
{
	s_screen = screen;
	s_goBack = goBack;
	needChange = true;
}


void GameEngine::ChangeScreen(CG::SCREENS screen, bool goBack)
{
	if (!goBack)
	{
		screen_manager.push_back(screen);
	}
	else
	{
		if (screen_manager.size() > 1)
		{
			screen_manager.pop_back();
			screen = screen_manager.at(screen_manager.size());
		}
	}

	Release();
	removeChild(currentScreen);
	currentScreen.reset();

	/*switch (screen)
	{
	case CG::MAIN_MENU_SCREEN:
	{
		currentScreen.reset(new MainMenu);
		break;
	}
	case CG::GAME_SCREEN:
	{
		break;
	}
	default:
		break;
	}*/

	if (screen == CG::MAIN_MENU_SCREEN)
	{
		currentScreen.reset(new MainMenu);
	}
	else if (screen == CG::GAME_SCREEN)
	{
		currentScreen.reset(new GameScreen);
	}

}

void GameEngine::Update()
{
	if(currentScreen != NULL)
	{
		currentScreen->Update();

		if (needChange)
		{
			ChangeScreen(s_screen, s_goBack);
			needChange= false;
		}
	}
}

void GameEngine::Draw()
{
	if (currentScreen != NULL)
	{
		currentScreen->Draw();
	}
}

void GameEngine::Release()
{
	if(currentScreen != NULL)
		currentScreen->Release();
}