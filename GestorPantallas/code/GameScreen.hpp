#ifndef _GAME_SCREEN_
#define _GAME_SCREEN_

#include "SSprite.hpp"
#include "SImage.hpp"
#include <iostream>
#include "SInput.hpp"
#include <IwGxPrint.h>
#include "SButton.hpp"
#include "STexture.hpp"
#include "SAnimSprite.hpp"
#include "Shared_Ptr.hpp"
#include "SText.hpp"
#include "SAudio.hpp"
#include "STweenManager.hpp"
#include "SDisplay.hpp"

using namespace storm;

class GameScreen : public SSprite
{
public:
	GameScreen();
	~GameScreen();

	void Update();
	void Release();

private:
	Shared_Ptr< STexture >	texture;
	Shared_Ptr< STexture >	texture2;
	Shared_Ptr< STexture >	texture3;
	Shared_Ptr< SImage >	image;
	Shared_Ptr< SImage >	image2;
	Shared_Ptr< SImage >	image3;
	Shared_Ptr< STexture > 	fondo;
	Shared_Ptr< SImage >	image_fondo;
};



#endif