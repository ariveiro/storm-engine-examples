#ifndef _GAME_
#define _GAME_

#include "SSprite.hpp"
#include "CG.hpp"
#include "vector"
#include "Shared_Ptr.hpp"
#include "MainMenu.hpp"
#include "GameScreen.hpp"

using storm::SSprite;
using std::vector;

class GameEngine : public SSprite
{
public:
	GameEngine();
	~GameEngine(){};

	void Update();
	void Draw();
	void Release();
	void ChangeScreen(CG::SCREENS screen, bool goBack);

	static void EventChangeScreen(CG::SCREENS screen, bool goBack);

private:
	static CG::SCREENS s_screen;
	static bool s_goBack;
	static bool needChange;

	vector<CG::SCREENS>		screen_manager;
	Shared_Ptr< SSprite >	currentScreen;

};


#endif